# Schüler Informations Webseite

Informations-Webseite für Schüler*innen, die an der Teilchenphysik Masterclass
der Uni Göttingen teilnehmen.

Die Webseite ist abrufbar unter:
https://masterclasses.gitlab.io/info-page/

Änderungen an den Texten können direkt über GitLab vorgenommen werden. Die meisten
Texte sind in dem Ordner `content` als markdown-Dateien, einige Meta-Daten sind
in der `config.toml` Datei gespeichert und müssen dort geändert werden. Wird eine
neue Datei im `content/post` Ordner hinzugefügt, erscheint sie auf der Webseite als
neuer Blog-Post.

Nach einer Änderung der Dateien im `master`-branch wird die Webseite sofort
automatisch aktualisiert.

Nähere technische Informationen siehe unten oder in der Hugo [Documentation][].

## Technisches

Diese Seite wird über GitLab Pages gehosted und basiert auf [Hugo][].

---

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

### Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
