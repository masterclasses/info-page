## Vorbereitung

Wir werden während der Masterclass das Programm `Hypathia` verwenden. Damit
wir alle ohne Probleme zusammenarbeiten können, führt bitte *vor* der
Masterclass die folgenden drei Schritte aus.

Sollten dabei irgendwelche Probleme auftauchen, kontaktiere uns bitte unter
masterclasses@uni-goettingen.de, damit wir gemeinsam das Problem vor den
Masterclasses lösen können.

### 1. Java Installieren

HYPATIA basiert auf JAVA. Das heißt, um HYPATIA zu starten, brauchst du eine
aktuelle JAVA Version, die du dir für dein Betriebssystem hier herunterladen
kannst:

https://www.java.com/de/download/

### 2. Hypathia Testen

Dann lädst du dir das HYPATIA Program über diesen Link herunter:

[Hypatia 7.4 Göttingen Version](/Hypatia_7.4_Masterclass_Goettingen.zip)

und entpackst den Ordner zum Beispiel auf dem Desktop. Wechsle in diesen
Ordner.

Als Windows und MAC Nutzer musst du nun einfach mit einem Doppelklick auf
`Hypatia_7.4_Masterclass.jar` das Programm starten.  Als Linux Benutzer öffnest
du ein Terminal in dem Ordner und rufst den folgenden Befehl auf:
```bash
source HYPATIA_for_Linux.sh
```

Es sollten sich vier Fenster öffnen, die so aussehen wie auf diesem Bild:

![](/hypatia_check_image.png)

### 3. Datensätze Herunterladen

Als letzten Schritt würden wir dich bitten die folgenden Datensätze
herunterzuladen und ebenfalls in deinem HYPATIA Ordner zu speichern:

- https://cernmasterclass.uio.no/datasets/allSets/dir05/
- https://cernmasterclass.uio.no/datasets/allSets/dir06/

Hat alles geklappt? Dann bist du bestens gerüstet für die Online Masterclasse
