---
title: Über Uns
subtitle: Was sind die Göttinger Teilchenphysik Masterclasses?
comments: false
---

Die Teilchenphysik Masterclasses des II. Physikalischen Instituts der
Universität Göttingen werden in Kooperation mit dem Netzwerk Teilchenwelt
veranstaltet. Sie sollen Schüler*innen die Chance geben, einen Einblick in die
Welt der Teilchenphysik zu bekommen. Dabei lernen sie nicht nur, "was die Welt
im Innersten zusammenhält", d.h. unseren aktuellen Kenntnisstand über die
fundamentalsten Betandteile des Universums, sonder auch die Methoden der
Teilchenphysik, wie zum Beispiel den LHC, das ATLAS Experiment und die
statistische Datenauswertung, kennen.
