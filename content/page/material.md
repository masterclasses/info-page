---
title: Material
subtitle: Wo kann ich mehr über Teilchenphysik erfahren?
comments: false
---

- Speziell zur Vorbereitung auf eine Masterclass hat die Uni Dresden die folgende
Webseite erstellt (ihr braucht das Passwort `Teilchenphysik!`):
https://bildungsportal.sachsen.de/opal/auth/RepositoryEntry/17635573762?0

- Die Uni Mainz hat eine Webseite mit interaktiven Spielen zu verschiedenen
Themen der Teilchenphysik:
https://online.schule.physik.uni-mainz.de/teilchenspiele/

- Wir sind Teil des Netzwerk Teilchenwelt, die auch an anderen Standorten
Veranstaltungen durchführen und über die ihr zum Beispiel auch ein Praktikum
am CERN in Genf machen könnt:
https://www.teilchenwelt.de/
